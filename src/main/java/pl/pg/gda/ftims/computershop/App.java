package pl.pg.gda.ftims.computershop;

import pl.pg.gda.ftims.computershop.components.*;
import pl.pg.gda.ftims.computershop.exception.NullObjectException;


public class App {
    public static void main(String[] args) {
        Catalog c1 = new Catalog();
        c1.addProduct(new MotherBoard(168,5,"Gigabyte Technology","Gigabyte Technology - ATX Motherboard 3466MHz (Socket LGA 1151)","ATX",64,"Socket LGA 1151" ));
        c1.addProduct(new MotherBoard(134.99,5,"Asus ","M5A99FX PRO R2.0 ATX Motherboard 2133MHz (Socket AM3+/AM3)","ATX",32,"Socket AM3+/AM3" ));
        c1.addProduct(new MotherBoard(109.99,5,"MSI","MSI - Enthusiast GAMING Series ATX Motherboard 2133MHz (Socket 1151) - Multi","ATX",64 ,"Socket 1151" ));
        c1.addProduct(new MotherBoard(249.99,5,"MSI","MSI - ATX Motherboard 3600MHz (Socket LGA1151)","ATX",64 ,"Socket 1151"));
        c1.addProduct(new Disk(148.99,5,"Seagate","Barracuda 4TB Internal SATA Hard Drive for Desktops - Silver",4000,600,600));
        c1.addProduct(new Disk(586.99,5,"Samsung ","Portable SSD T3 1TB External USB 3.1 Gen1 Portable Hard Drive - Black/Silver",1000,600,600));
        c1.addProduct(new Disk(148.99,5,"WD","Purple 1TB Internal Serial ATA Hard Drive (OEM/Bare Drive) - Silver",1000,768,768));
        c1.addProduct(new Disk(199.99,5,"Samsung","Portable SSD T3 500GB External USB 3.1 Gen1 Portable Hard Drive - Black/Silver",500,450,450));
        c1.addProduct(new RamMemory(34.99,5,"PNY","4GB DDR3 SDRAM Memory Module","DDR3",4,1333));
        c1.addProduct(new RamMemory(192.99,5,"Corsair","Vengeance LPX 2-Pack 16GB DDR4 DRAM Desktop Memory Kit - Black","DDR4",16,2666));
        c1.addProduct(new RamMemory(69.99,5,"Crucial"," 2-Pack 4GB 1.1 GHz DDR3 SoDIMM Laptop Memory Kit","DDR3",4,1100));
        c1.addProduct(new RamMemory(67.99,5,"PNY","8GB PC3-12800 DDR3 DIMM Desktop Memory - Green","DDR3",8,1600));
        c1.addProduct(new Procesor(349.99,5,"Intel","Core™ i7-6700K 4.0GHz Processor - Silver",4.0,"Socket LGA 1151",4));
        c1.addProduct(new Procesor(314.99,5,"Intel","Core i7-6700 3.4GHz Processor - Silver",3.4,"Socket LGA 1151",4));
        c1.addProduct(new Procesor(175.99,5,"AMD","FX-8350 4.0GHz Processor - Black",4.0,"Socket AM3+",8));
        c1.addProduct(new Procesor(61.99,5,"AMD","A6-7400K 3.5GHz Processor - Black",4.0,"Socket FM2+",6));

        MainManu m1 = new MainManu(c1);
        try {
            m1.menu();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
