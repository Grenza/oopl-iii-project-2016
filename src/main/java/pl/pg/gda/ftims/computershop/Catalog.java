package pl.pg.gda.ftims.computershop;

import pl.pg.gda.ftims.computershop.components.*;
import pl.pg.gda.ftims.computershop.exception.NullObjectException;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;


public class Catalog {
    HashMap<String,List<Product>> products;
    private final List<String> compontents = new LinkedList<String>() {{add("Disk"); add("RamMemory"); add("Procesor"); add("MotherBoard"); }};
    private int nextID;


    public Catalog() {
        this.products = new HashMap<String, List<Product>>();
        for (String el: compontents) {
            products.put(el,new LinkedList<Product>());
        }
        this.nextID =0;
    }

    public void addProduct(Product product){
        String componentName = product.getClass().getSimpleName();
        products.get(componentName).add(product);
        this.nextID++;
    }

    public void addProduct(){
        List<Product> category= null;
        try {
            category = chooseTheCategory();
        }
        catch(Exception e) {
            e.getMessage();
        }
        if (!(category.isEmpty())){
            String typeOfCategory = category.get(0).getClass().getSimpleName();
            int indexOfChosenCategory = compontents.indexOf(typeOfCategory);
            switch (indexOfChosenCategory){
                case 0:
                    category.add(new Disk());
                    break;
                case 1:
                    category.add(new RamMemory());
                    break;
                case 2:
                    category.add(new Procesor());
                    break;
                case 3:
                    category.add(new MotherBoard());
            }
        }
        this.nextID++;
    }

    public Product searchProduct(){
        List<Product> category= null;
        int numberChoosenByUser = -1;
        try {
           category = chooseTheCategory();
        }
        catch(Exception e) {
            e.getMessage();
        }
        int counter=0;
        Scanner scanner = new Scanner(System.in);
        for (Product el: category) {
            System.out.println("["+counter+"]"+ " " + el);
            counter++;
        }
        System.out.println("Chose the numer of the product");
        numberChoosenByUser=-1;
        while (!(numberChoosenByUser>=0&&numberChoosenByUser<compontents.size())) {
            numberChoosenByUser = scanner.nextInt();
        }
        return category.get(numberChoosenByUser);

        }

    private List<Product> chooseTheCategory() throws NullObjectException{
        System.out.println("Chose the type of the component");
        int numberOfComponentType =0;
        int numberChoosenByUser =-1;
        List<Product> result;
        Scanner scaner = new Scanner(System.in);

        for (String el: compontents) {
            System.out.println(numberOfComponentType + " " + el);
            numberOfComponentType++;
        }
        while(!(numberChoosenByUser>=0 && numberChoosenByUser<compontents.size())) {
            numberChoosenByUser = scaner.nextInt();
        }

        switch (numberChoosenByUser){
            case 0:
                result = products.get("Disk");
                break;
            case 1:
                result =  products.get("RamMemory");
                break;
            case 2:
                result = products.get("Procesor");
                break;
            case 3:
                result = products.get("MotherBoard");
                break;
            default:
                result=null;
                break;
        }
        if (result==null){
            throw new NullObjectException();
        }
        return result;
    }

    public void printTheCatergory(){
        List<Product> category = null;
        try {
            category = chooseTheCategory();
        }
        catch(Exception e) {
            e.getMessage();
        }
        System.out.println(category);
    }


    @Override
    public String toString() {
        String result ="Catalog \n";
        for (String el: compontents) {
            result += products.get(el);
        }
        return result+ "\n";
    }
}
