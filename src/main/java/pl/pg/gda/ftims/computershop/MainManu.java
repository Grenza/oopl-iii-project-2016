package pl.pg.gda.ftims.computershop;

import pl.pg.gda.ftims.computershop.components.Product;

import java.io.IOException;
import java.util.Scanner;


public class MainManu {
private Catalog catalog;

    public MainManu(Catalog catalog) {
        this.catalog = catalog;
    }

    public void menu(){
        boolean exit = false;
        Scanner scanner = new Scanner(System.in);
        int numberChoosenByUser;

        while (!(exit == true)) {
            System.out.println("[1]Show all products");
            System.out.println("[2]Show all products in category");
            System.out.println("[3]Buy");
            System.out.println("[4]Sell");
            System.out.println("[5]Add product");
            System.out.println("[6]Exit");
            numberChoosenByUser = scanner.nextInt();
            switch (numberChoosenByUser){
                case 1:
                    System.out.println(catalog);
                    System.out.println("\n");
                    break;
                case 2:
                    catalog.printTheCatergory();
                    System.out.println("\n");
                     break;
                case 3:
                    Product product = catalog.searchProduct();
                    System.out.println("How many would like to buy");
                    int quantity = scanner.nextInt();
                    product.buy(quantity);
                    System.out.println("You bought " + quantity + " " + product.getName() + " \n");
                    break;
                case 4:
                    Product product2 = catalog.searchProduct();
                    System.out.println("How many would like to sell");
                    int quantity2 = scanner.nextInt();
                    product2.buy(quantity2);
                    System.out.println("You sold " + quantity2 + " " + product2.getName() + " \n");
                    break;
                case 5:
                    catalog.addProduct();
                    System.out.println("\n");
                    break;
                case 6:
                    exit=true;
                    break;
            }

        }

    }


}
