package pl.pg.gda.ftims.computershop.components;

import pl.pg.gda.ftims.computershop.exception.ProductNoAvailableException;
import pl.pg.gda.ftims.computershop.interfaces.ShopOperations;

import java.util.Scanner;

public class Disk extends Product implements ShopOperations {
    private int size;
    private int readSpeed;
    private int writeSpeed;

    public Disk(double prize, int quantity, String producer, String name, int size, int readSpeed, int writeSpeed) {
        super(prize, quantity, producer, name);
        this.size = size;
        this.readSpeed = readSpeed;
        this.writeSpeed = writeSpeed;

    }

    public Disk(){
        super();
        Scanner scanner = new Scanner(System.in);
        System.out.print("Size: ");
        this.size = scanner.nextInt();
        System.out.print("Read Speed: ");
        this.writeSpeed = scanner.nextInt();
        System.out.print("Write Speed: ");
        this.readSpeed = scanner.nextInt();
    }

    public int getsize() {
        return size;
    }

    public void setsize(int size) {
        this.size = size;
    }

    public int getReadSpeed() {
        return readSpeed;
    }

    public void setReadSpeed(int readSpeed) {
        this.readSpeed = readSpeed;
    }

    public int getWriteSpeed() {
        return writeSpeed;
    }

    public void setWriteSpeed(int writeSpeed) {
        this.writeSpeed = writeSpeed;
    }

    @Override
    public String toString() {
        return "Disk {" +
                "\nname: " + super.getName() +
                "\nproducer: " + super.getProducer() +
                "\nprize: " + super.getPrize() +
                "\nquantity: " + super.getQuantity() +
                "\nsize: " + size +
                "\nreadSpeed: " + readSpeed +
                "\nwriteSpeed: " + writeSpeed +
                "}\n\n";
    }

    public void edit(){
        Scanner scanner = new Scanner(System.in);
        System.out.print(this.getName());
        this.setName(scanner.nextLine());
        System.out.print(this.getProducer());
        this.setProducer(scanner.nextLine());
        System.out.print(this.getQuantity());
        this.setQuantity(scanner.nextInt());
        System.out.print(this.getPrize());
        this.setPrize(scanner.nextDouble());
        System.out.print(this.getsize());
        this.setsize(scanner.nextInt());
        System.out.print(this.getWriteSpeed());
        this.setWriteSpeed(scanner.nextInt());
        System.out.print(this.getReadSpeed());
        this.setReadSpeed(scanner.nextInt());
    }

}
