package pl.pg.gda.ftims.computershop.components;

import java.util.Scanner;

public class MotherBoard extends Product{
    String formfactor;
    int maximymMemorySuported;
    String processorSoket;

    public MotherBoard(double prize, int quantity, String producer, String name, String formfactor, int maximymMemorySuported, String processorSoket) {
        super(prize, quantity, producer, name);
        this.formfactor = formfactor;
        this.maximymMemorySuported = maximymMemorySuported;
        this.processorSoket = processorSoket;
    }

    public MotherBoard() {
        super();
        Scanner scanner = new Scanner(System.in);
        System.out.print("Frorm Factor");
        this.formfactor = scanner.nextLine();
        System.out.print("Maximum Memmory Suported");
        this.maximymMemorySuported = scanner.nextInt();
        System.out.print("Processor Soket");
        this.processorSoket = scanner.nextLine();
    }

    public String toString() {
        return  "MotherBoard {"+
                "\nName: " + super.getName() +
                "\nProducer: " + super.getProducer() +
                "\nPrize: " + super.getPrize() +
                "\nQuantity: " + super.getQuantity() +
                "\nForm Factor " + formfactor +
                "\nMaximum Memory Suported: " + maximymMemorySuported +
                "\nProcessor Soket: " + processorSoket +
                "}\n\n";
    }

    public String getFormfactor() {
        return formfactor;
    }

    public void setFormfactor(String formfactor) {
        this.formfactor = formfactor;
    }

    public int getMaximymMemorySuported() {
        return maximymMemorySuported;
    }

    public void setMaximymMemorySuported(int maximymMemorySuported) {
        this.maximymMemorySuported = maximymMemorySuported;
    }

    public String getProcessorSoket() {
        return processorSoket;
    }

    public void setProcessorSoket(String processorSoket) {
        this.processorSoket = processorSoket;
    }

}
