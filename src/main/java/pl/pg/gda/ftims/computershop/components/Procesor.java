package pl.pg.gda.ftims.computershop.components;

import java.util.Scanner;

public class Procesor extends Product{
    private double speed;
    private String socket;
    private int prcessorCores;

    public Procesor(double prize, int quantity, String producer, String name, double speed, String socket, int prcessorCores) {
        super(prize, quantity, producer, name);
        this.speed = speed;
        this.socket = socket;
        this.prcessorCores = prcessorCores;
    }

    public Procesor() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Speed: ");
        this.speed=scanner.nextDouble();
        System.out.print("Socket: ");
        this.socket=scanner.nextLine();
        System.out.print("Proccesor Cores: ");
        this.prcessorCores = scanner.nextInt();
    }

    public String toString() {
        return  "Procesor {"+
                "\nName: " + super.getName() +
                "\nProducer: " + super.getProducer() +
                "\nPrize: " + super.getPrize() +
                "\nQuantity: " + super.getQuantity() +
                "\nSpeed: " + speed +
                "\nSocket: " + socket +
                "\nprocessorCores: " + prcessorCores  +
                "}\n\n";
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public String getSocket() {
        return socket;
    }

    public void setSocket(String socket) {
        this.socket = socket;
    }

    public int getPrcessorCores() {
        return prcessorCores;
    }

    public void setPrcessorCores(int prcessorCores) {
        this.prcessorCores = prcessorCores;
    }
}
