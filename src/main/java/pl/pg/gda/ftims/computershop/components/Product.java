package pl.pg.gda.ftims.computershop.components;

import pl.pg.gda.ftims.computershop.exception.ProductNoAvailableException;
import pl.pg.gda.ftims.computershop.interfaces.ShopOperations;

import java.util.Scanner;


public abstract class Product implements ShopOperations {
    private double prize;
    private int quantity;
    private String producer;
    private String name;
    private int id;

    public Product(double prize, int quantity, String producer, String name) {
        this.prize = prize;
        this.quantity = quantity;
        this.producer = producer;
        this.name = name;
    }

    public Product() {
        Scanner scaner = new Scanner(System.in);
        System.out.print("Name Product: ");
        this.name= scaner.nextLine();
        System.out.print("Producer: ");
        this.producer= scaner.nextLine();
        System.out.print("Prize: ");
        this.prize= scaner.nextDouble();
        System.out.print("Quantity: ");
        this.quantity= scaner.nextInt();
    }

    public double getPrize() {
        return prize;
    }

    public void setPrize(double prize) {
        this.prize = prize;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void sell() throws ProductNoAvailableException {
        if (this.quantity>0)
        this.quantity--;
        else
        throw new ProductNoAvailableException();
    }

    public void buy() {
        this.quantity++;
    }

    public void sell(int quantity) throws ProductNoAvailableException{
        if (this.quantity-quantity>=0)
            this.quantity+=quantity;
        else
            throw new ProductNoAvailableException();
    }

    public void buy(int quantity) {
        this.quantity+=quantity;
    }
}
