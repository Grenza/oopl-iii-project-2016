package pl.pg.gda.ftims.computershop.components;

import java.util.Scanner;


public class RamMemory extends Product {
    private String type;
    private int capacity;
    private int speed;

    public RamMemory(double prize, int quantity, String producer, String name,String type,int capacity,int speed) {
        super(prize, quantity, producer, name);
        this.capacity = capacity;
        this.speed = speed;
        this.type = type;
    }

    public RamMemory(){
        super();
        Scanner scanner = new Scanner(System.in);
        System.out.print("Capacity: ");
        this.capacity = scanner.nextInt();
        System.out.print("Speed: ");
        this.speed = scanner.nextInt();
        System.out.print("Type: ");
        this.type = scanner.nextLine();
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getcapacity() {
        return capacity;
    }

    public void setcapacity(int capacity) {
        this.capacity = capacity;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public String toString() {
        return  "RamMemory {"+
                "\nName: " + super.getName() +
                "\nProducer: " + super.getProducer() +
                "\nPrize: " + super.getPrize() +
                "\nQuantity: " + super.getQuantity() +
                "\nSpeed: " + speed +
                "\nType: " + type +
                "\nCapacity: " + capacity +
                "}\n\n";
    }



}
