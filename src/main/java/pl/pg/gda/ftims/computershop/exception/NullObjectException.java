package pl.pg.gda.ftims.computershop.exception;


public class NullObjectException extends Exception {
    @Override
    public String getMessage() {
        return "Chosen option is wrong";
    }
}
