package pl.pg.gda.ftims.computershop.exception;


public class ProductNoAvailableException extends Exception {
    @Override
    public String getMessage() {
        return "Product currently is not avaiblable";
    }
}
