package pl.pg.gda.ftims.computershop.interfaces;

import pl.pg.gda.ftims.computershop.exception.ProductNoAvailableException;

/**
 * Created by jakgr_000 on 2017-01-20.
 */
public interface ShopOperations {
    public void sell() throws ProductNoAvailableException;
    public void buy();
    public void sell(int quantity) throws ProductNoAvailableException;
    public void buy(int quantity);
}
